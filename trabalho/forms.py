# -*- coding: utf-8 -*-
from django import forms
from .models import Trabalho
from django_select2.forms import Select2Widget, Select2MultipleWidget

class TrabalhoForm(forms.ModelForm):

	TIPO = (
		('', ''),
		('AR', 'Artigo Resumido'),
		('AC', 'Artigo Completo'),
		('AP', 'Artigo IHC na Prática')
	)

	key = forms.CharField(widget=forms.HiddenInput(), required=False)
	titulo = forms.CharField(label='Título', widget=forms.TextInput(attrs={'class': 'form-control'}))
	tipo = forms.CharField(label='Tipo', widget=Select2Widget(choices=TIPO, attrs={'class': 'form-control'}), required=False)
	ordem = forms.CharField(label='Ordem', widget=forms.NumberInput(attrs={'class': 'form-control', 'min': 1, 'max': 20}))
	sessao = forms.CharField(label='Sessão', widget=Select2Widget)
	sessao_antiga = forms.CharField(widget=forms.HiddenInput(), required=False)
	autores = forms.CharField(label='Autores', widget=forms.SelectMultiple(attrs={'data-role': 'tagsinput', 'class': 'form-control'}))

	class Meta:
		model = Trabalho
		fields = '__all__'
