from django.conf.urls import url

from .views import Trabalho, trabalhos, excluir_trabalho

urlpatterns = [
	url(r'^trabalho/$', Trabalho.as_view(), name='trabalho'),
  url(r'^trabalhos/$', trabalhos, name='trabalhos'),
  url(r'^excluir_trabalho/$', excluir_trabalho, name='excluir_trabalho'),
]