# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import ast
from django.core.validators import MaxValueValidator, MinValueValidator

from django.db import models
from firebase.backend import firebase

TIPO = (
	('AR', 'Artigo Resumido'),
	('AC', 'Artigo Completo'),
	('AP', 'Artigo IHC na Prática')
)

class Trabalho(models.Model):
	class Meta:
		verbose_name = "Trabalho"
		verbose_name_plural = "Trabalhos"

	key = models.CharField(max_length=30, editable=False)
	titulo = models.CharField(max_length=400)
	tipo = models.CharField(max_length=25, blank=True)
	ordem = models.PositiveIntegerField(validators=[MaxValueValidator(20), MinValueValidator(1)])
	sessao = models.CharField(max_length=30)
	sessao_antiga = models.CharField(max_length=30, blank=True)
	autores = models.CharField(max_length=400)

	def __unicode__(self):
		return self.titulo

	def save(self, *args, **kwargs):
		db = firebase.database()

		autores = ast.literal_eval(self.autores)
		tipo = ''
		for i in range(len(TIPO)):
			if TIPO[i][0] == self.tipo:
				tipo = TIPO[i][1]
				break

		dados = {
			'titulo': self.titulo,
			'tipo': tipo,
			'ordem': self.ordem,
			'autores': autores
		}

		if not self.key:
			salvo = db.child('sessoes').child(self.sessao).child('trabalhos').push(dados)
			self.key = salvo['name']
		else:
			if self.sessao != self.sessao_antiga:
				db.child('sessoes').child(self.sessao_antiga).child('trabalhos').child(self.key).remove()
			db.child('sessoes').child(self.sessao).child('trabalhos').child(self.key).update(dados)

	def delete(self):
		db = firebase.database()
		db.child('sessoes').child(self.sessao).child('trabalhos').child(self.key).remove()
