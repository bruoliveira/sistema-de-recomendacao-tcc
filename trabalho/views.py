# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import json

from django.shortcuts import render, redirect
from django.http import Http404, HttpResponseForbidden, HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.views import View
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from firebase.backend import firebase
from .models import Trabalho
from .forms import TrabalhoForm

logger = logging.getLogger(__name__)

TIPO = (
	('AR', 'Artigo Resumido'),
	('AC', 'Artigo Completo'),
	('AP', 'Artigo IHC na Prática')
)

class Trabalho(View):
	@method_decorator(login_required)
	def get(self, request):
		db = firebase.database()
		# pega as sessões para popular no template
		sessoes_db = db.child('sessoes').get()
		sessoes = []
		for t in sessoes_db.each():
			try:
				sessao = {}
				sessao['text'] = t.val()['nome']
				sessao['id'] = t.key()
				sessoes.append(sessao)
			except Exception as e:
				logger.warning('Erro com a Sessão: %s', e)
		sessoes_sorted = sorted(sessoes, key=lambda k: k['text'])
		sessoes_sorted.insert(0, {'id': '', 'text': ''})
		# inicia a criação do formulário
		try:
			# caso exista uma key, será retornado um form populado com as informações do firebase para edição
			key = request.GET.get('key')
			sessao_selecionada = ''
			for sessao in sessoes_db.each():
				try:
					for trabalho in sessao.val()['trabalhos'].items():
						if trabalho[0] == key:
							sessao_selecionada = sessao.key()
				except Exception as e:
					pass

			trabalho_db = db.child('sessoes').child(sessao_selecionada).child('trabalhos').child(key).get().val()

			tipo = ''
			for i in range(len(TIPO)):
				if TIPO[i][1] == trabalho_db['tipo']:
					tipo = TIPO[i][0]
					break

			# popula o form com as informações da sessão
			form = TrabalhoForm(
				initial={
					'key': key,
					'titulo': trabalho_db['titulo'],
					'ordem': trabalho_db['ordem'],
					'sessao_antiga': sessao_selecionada
				}
			)
			# prepara o conteúdo que vai pro template
			# pra chamar no template tem que usar o nome que está entre aspas simples
			contexto = {
				'form': form,
				'sessao_selecionada': sessao_selecionada,
				'tipo': tipo,
				'autores': trabalho_db['autores'],
				'sessoes': json.dumps(sessoes_sorted),
			}
		except Exception as e:
			# do contrário, será retornado um form vazio, que será usado para a criação de um novo objeto
			form = TrabalhoForm()

			contexto = {
				'form': form,
				'sessoes': json.dumps(sessoes_sorted),
			}

		return render(request, 'trabalho.html', contexto)

	@method_decorator(login_required)
	def post(self, request):
		form = TrabalhoForm(request.POST or None)
		if form.is_valid():
			#vai pegar a key, mesmo que ela seja vazia
			#caso a key seja vazia, é sinal de que o objeto é novo
			key = form.cleaned_data['key']
			#cria um objeto mas não salva, pois o commit=False
			trabalho = form.save(commit=False)
			#Esse objeto não tem uma key definida, mas a key existe no formulário
			#Então é necessário passar a key da sessão editada para que uma nova sessão não seja criada
			trabalho.key = key
			#salva o objeto editado
			trabalho.save()
			logger.info('Trabalho salvo com sucesso: %s', trabalho.key)
			messages.success(request, 'Trabalho salvo com sucesso!')

			if 'salvar' in request.POST:
				url = reverse('trabalho')
				return redirect(url + '?key=' + trabalho.key)
			elif 'salvar_novo' in request.POST:
				return redirect('trabalho')
			else:
				return redirect('trabalhos')
		else:
			contexto = {'form': form}
			return render(request, 'trabalho.html', contexto)

@login_required
def excluir_trabalho(request):
	if request.method == 'POST':
		db = firebase.database()
		item = request.POST.get('item')
		sessao = request.POST.get('sessao')
		trabalho = db.child('sessoes').child(sessao).child('trabalhos').child(item).get().val()
		if trabalho:
			db.child('sessoes').child(sessao).child('trabalhos').child(item).remove()
			messages.success(request, 'Trabalho removido com sucesso!')
			return HttpResponse('True')
		else:
			messages.error(request, 'Erro ao remover o trabalho!')
			return HttpResponse('False')
	else:
		return redirect('trabalhos')

@login_required
def trabalhos(request):
	db = firebase.database()
	sessoes_fb = db.child('sessoes').get()

	trabalhos = []
	for sessao in sessoes_fb.each():
		try:
			trabalhos_fb = sessao.val()['trabalhos']
			for key, trabalho in trabalhos_fb.items():
				try:
					novo_trabalho = {}
					novo_trabalho['key'] = key
					novo_trabalho['titulo'] = trabalho['titulo']
					novo_trabalho['sessao'] = sessao.val()['nome']
					novo_trabalho['sessao_key'] = sessao.key()
					novo_trabalho['tipo'] = trabalho['tipo']
					trabalhos.append(novo_trabalho)
				except KeyError as e:
					logger.error('Não foi possível carregar o trabalho: %s', e)
		except TypeError as e:
			logger.info('Não existem trabalhos na sessão. Erro: %s', e)
		except KeyError as e:
			logger.info('Não existem trabalhos na sessão. Erro: %s', e)

	# Cria o sistema de paginação
	paginator = Paginator(trabalhos, 20) # mostra 20 trabalhos por página
	pagina = request.GET.get('page')

	try:
		lista_trabalhos = paginator.page(pagina)
	except PageNotAnInteger:
		# se a página não for um inteiro, mostra a primeira
		pagina = 1
	except EmptyPage:
		# se a página for fora do intervalo, mostra a primeira
		if int(pagina) <= 0:
			pagina = 1
		else:
			pagina = paginator.num_pages

	pagina = int(pagina)
	lista_trabalhos = paginator.page(pagina)

	range_min = 1 if (pagina - 1 <= 0) else (pagina - 1)
	range_max = paginator.num_pages + 1 if (pagina + 2 > paginator.num_pages) else (pagina + 2)
	range_paginas = xrange(range_min, range_max)

	contexto = {'trabalhos': lista_trabalhos, 'range_paginas': range_paginas, 'penultima_pagina': paginator.num_pages-1}

	return render(request, 'trabalhos.html', contexto)
