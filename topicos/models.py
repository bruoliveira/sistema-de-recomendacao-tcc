# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from firebase.backend import firebase

# Create your models here.
class TopicoInteresse(models.Model):
  class Meta:
    verbose_name = 'Tópico de Interesse'
    verbose_name_plural = 'Tópicos de Interesse'

  nome = models.CharField(max_length=100)
  key = models.CharField(max_length=30, editable=False)

  def __unicode__(self):
    return self.nome

  def save(self, *args, **kwargs):
    db = firebase.database()
    dados = {
      'nome': self.nome,
    }
    if not self.key:
      try:
        ultimos = db.child('topicos_interesse').get()
        ultimo = ultimos.each()[-1]
        key = ultimo.key() + 1
      except Exception as e:
        key = 0
      self.key = key
    db.child('topicos_interesse').update({self.key: self.nome})

  def delete(self):
    db = firebase.database()
    db.child('topicos_interesse').child(self.key).remove()