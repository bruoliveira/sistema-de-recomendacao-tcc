# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views import View
from django.core.urlresolvers import reverse
from django.utils.decorators import method_decorator

from .models import TopicoInteresse
from .forms import TopicoInteresseForm

from django.http import JsonResponse
from django.contrib import messages
from firebase.backend import firebase

# Create your views here.
class Topico(View):
  @method_decorator(login_required)
  def get(self, request):
    try:
      # caso exista uma key, será retornado um form populado com as informações do firebase para edição
      key = request.GET.get('key')
      db = firebase.database()
      topico = db.child('topicos_interesse').child(key).get().val()

      form = TopicoInteresseForm(
        initial={
          'key': key,
          'nome': topico,
        }
      )
    except Exception as e:
      # do contrário, será retornado um form vazio, que será usado para a criação de um novo objeto
      form = TopicoInteresseForm()

    contexto = {'form': form}
    return render(request, 'cadastrar_topico.html', contexto)

  @method_decorator(login_required)
  def post(self, request):
    form = TopicoInteresseForm(request.POST or None)
    if form.is_valid():
      # vai pegar a key, mesmo que ela seja vazia
      # caso a key seja vazia, é sinal de que o objeto é novo
      key = form.cleaned_data['key']
      # cria um objeto mas não salva, pois o commit=False
      topico = form.save(commit=False)
      # Esse objeto não tem uma key definida, mas a key existe no formulário
      # Então é necessário passar a key da sessão editada para que uma nova sessão não seja criada
      topico.key = key
      # salva o objeto editado
      topico.save()
      messages.success(request, 'Tópico salvo com sucesso!')

      if 'salvar' in request.POST:
        url = reverse('topico')
        return redirect(url + '?key=' + unicode(topico.key))
      elif 'salvar_novo' in request.POST:
        return redirect('topico')
      else:
        return redirect('topicos')
    else:
      contexto = {'form': form}
      return render(request, 'cadastrar_topico.html', contexto)

@login_required
def excluir_topico(request):
  if request.method == 'POST':
    db = firebase.database()
    item = request.POST.get('item')
    topico = db.child('topicos_interesse').child(item).get()
    if topico:
      db.child('topicos_interesse').child(item).remove()
      messages.success(request, 'Tópico removido com sucesso!')
      return HttpResponse('True')
    else:
      messages.error(request, 'Erro ao remover o tópico!')
      return HttpResponse('False')
  else:
    return redirect('topicos')

@login_required
def topicos(request):
  db = firebase.database()
  topicos_db = db.child('topicos_interesse').get()
  topicos = []
  try:
    for tp in topicos_db.each():
      try:
        topico = {}
        topico['key'] = tp.key()
        topico['nome'] = tp.val()
        topicos.append(topico)
      except KeyError as e:
        pass
  except AttributeError as e:
    messages.info(request, 'Não existem tópicos cadastrados!')

  contexto = {'topicos': topicos}

  return render(request, 'topicos.html', contexto)
