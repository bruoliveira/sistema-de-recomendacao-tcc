# -*- coding: utf-8 -*-
from django import forms
from .models import TopicoInteresse

class TopicoInteresseForm(forms.ModelForm):
  key = forms.CharField(widget=forms.HiddenInput(),required=False)
  nome = forms.CharField(label='Nome', widget=forms.TextInput(attrs={'class': 'form-control'}))

  def clean_key(self):
    key = self.cleaned_data['key']
    if key == 'None':
      key = None
    return key

  class Meta:
    model = TopicoInteresse
    fields = '__all__'