from django.conf.urls import url

from .views import topicos, Topico, excluir_topico

urlpatterns = [
  url(r'^topico/$', Topico.as_view(), name='topico'),
  url(r'^topicos/$', topicos, name='topicos'),
  url(r'^excluir_topico/$', excluir_topico, name='excluir_topico'),
]