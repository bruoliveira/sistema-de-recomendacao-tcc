from django.conf.urls import url

from django.contrib.auth import views
from .forms import LoginForm, UpdatePasswordForm

urlpatterns = [
  url(r'^login/$', views.login, {'template_name': 'login.html', 'authentication_form': LoginForm}, name='login'),
  url(r'^trocar_senha/$', views.password_change,
    {
      'template_name': 'trocar_senha.html',
      'password_change_form': UpdatePasswordForm,
      'post_change_redirect': 'change_password',
    },
    name='change_password'),
  url(r'^logout/$', views.logout, {'next_page': 'login'}, name='logout'),
]
