# -*- coding: utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm, SetPasswordForm
from django.contrib.auth.models import User
from django import forms

# If you don't do this you cannot use Bootstrap CSS
class LoginForm(AuthenticationForm):
  username = forms.CharField(label="Username", max_length=30,
                widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'name': 'username', 'placeholder': 'Nome de Usuario'}))
  password = forms.CharField(label="Password", max_length=30,
                widget=forms.PasswordInput(attrs={'class': 'form-control input-lg', 'type':'password', 'name': 'password', 'placeholder': 'Senha'}))

class UpdatePasswordForm(SetPasswordForm):

  old_password = forms.CharField(label="Senha Antiga",
              widget=forms.PasswordInput(attrs={'class': 'form-control input-lg', 'name': 'old_password', 'placeholder': 'Senha Antiga'}))
  new_password1 = forms.CharField(label="Nova Senha",
              widget=forms.PasswordInput(attrs={'class': 'form-control input-lg', 'name': 'new_password1', 'placeholder': 'Nova Senha'}))
  new_password2 = forms.CharField(label="Confirme a nova senha",
              widget=forms.PasswordInput(attrs={'class': 'form-control input-lg', 'name': 'new_password2', 'placeholder': 'Repita a Nova Senha'}))
