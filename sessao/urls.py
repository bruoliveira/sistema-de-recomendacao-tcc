from django.conf.urls import url
from django.shortcuts import redirect
from .views import sessoes, Sessao, excluir_sessao

urlpatterns = [
	url(r'^sessao/$', Sessao.as_view(), name='sessao'),
	url(r'^sessoes/$', sessoes, name='sessoes'),
	url(r'^excluir_sessao/$', excluir_sessao, name='excluir_sessao'),
  url(r'^$', lambda x: redirect(sessoes), name='home'),
]
