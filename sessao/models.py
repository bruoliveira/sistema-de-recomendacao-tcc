# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from firebase.backend import firebase
from datetime import datetime
import re

class Sessao(models.Model):
	class Meta:
		verbose_name = "Sessão"
		verbose_name_plural = "Sessões"

	nome = models.CharField(max_length=300)
	descricao = models.TextField()
	capacidade = models.IntegerField()
	sala = models.CharField(max_length=25)
	tipo = models.CharField(max_length=70)
	data = models.DateField()
	hora_inicio = models.TimeField()
	hora_fim = models.TimeField()
	key = models.CharField(max_length=30, editable=False)
	topicos_interesse = models.TextField()

	def __unicode__(self):
		return self.nome

	def save(self, *args, **kwargs):
		db = firebase.database()
		timestamp_start = datetime.combine(self.data, self.hora_inicio)
		timestamp_end = datetime.combine(self.data, self.hora_fim)
		topicos_db = db.child('topicos_interesse').get()

		# pega a string dos tópicos selecionados para a sessão e transforma em uma lista, usando expressão regular
		tp = re.findall(r"'(.*?)'", self.topicos_interesse, re.DOTALL)
		topicos_interesse = [ int(x) for x in tp ]

		# cria o dict que armazena as informações dos tópicos da sessão
		topicos = {}
		for topico in topicos_interesse:
			topicos[topico] = topicos_db.val()[topico]

		dados = {
			'nome': self.nome,
			'descricao': self.descricao,
			'capacidade': self.capacidade,
			'sala': self.sala,
			'tipo': self.tipo,
			'timestamp_start': long(timestamp_start.strftime("%s")),
			'timestamp_end': long(timestamp_end.strftime("%s")),
			'topicos_interesse': topicos
		}

		if not self.key:
			salvo = db.child('sessoes').push(dados)
			self.key = salvo['name']
		else:
			db.child('sessoes').child(self.key).update(dados)

	def delete(self):
		db = firebase.database()
		db.child('sessoes').child(self.key).remove()
