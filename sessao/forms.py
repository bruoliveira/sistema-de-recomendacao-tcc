# -*- coding: utf-8 -*-
from django import forms
from .models import Sessao
from django_select2.forms import Select2MultipleWidget

class SessaoForm(forms.ModelForm):

	key = forms.CharField(widget=forms.HiddenInput(), required=False)
	nome = forms.CharField(label='Nome', widget=forms.TextInput(attrs={'class': 'form-control'}))
	descricao = forms.CharField(label='Descrição', widget=forms.Textarea(attrs={'class': 'form-control'}))
	capacidade = forms.DecimalField(label='Capacidade', widget=forms.TextInput(attrs={'class': 'form-control'}))
	sala = forms.CharField(label='Sala', widget=forms.TextInput(attrs={'class': 'form-control'}))
	tipo = forms.CharField(label='Tipo', widget=forms.TextInput(attrs={'class': 'form-control'}))
	data = forms.DateField(label='Data', widget=forms.DateInput(attrs={'class': 'form-control'}))
	hora_inicio = forms.TimeField(label='Hora de Início', widget=forms.TimeInput(attrs={'class': 'form-control'}))
	hora_fim = forms.TimeField(label='Hora de Término', widget=forms.TimeInput(attrs={'class': 'form-control'}))
	topicos_interesse = forms.CharField(label='Tópicos de Interesse', widget=Select2MultipleWidget, required=False)

	class Meta:
		model = Sessao
		fields = '__all__'
