# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
import json

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.views import View
from django.core.urlresolvers import reverse
from datetime import datetime

from firebase.backend import firebase
from .models import Sessao
from .forms import SessaoForm

logger = logging.getLogger(__name__)

class Sessao(View):
	@method_decorator(login_required)
	def get(self, request):
		db = firebase.database()
		# pega os tópicos para popular as options do select no template
		topicos_db = db.child('topicos_interesse').get()
		topicos_interesse = []
		for t in topicos_db.each():
			try:
				topico = {}
				topico['text'] = t.val()
				topico['id'] = t.key()
				topicos_interesse.append(topico)
			except Exception as e:
				logger.warning('Erro com o Tópico: %s', e)

		# inicia a criaçao do formulário
		try:
			# caso exista uma key, será retornado um form populado com as informações do firebase para edição
			key = request.GET.get('key')
			sessao_db = db.child('sessoes').child(key).get().val()

			topicos_sessao = []
			try:
				topicos_sessao_db = db.child('sessoes').child(key).child('topicos_interesse').get()
				# pega os tópicos da sessão para popular o select no template
				for tp in topicos_sessao_db.each():
					topicos_sessao.append(int(tp.key()))
			except:
				logger.error('Não existem tópicos para essa sessão')

			# popula o form com as informações da sessão
			form = SessaoForm(
				initial={
					'key': key,
					'nome': sessao_db['nome'],
					'descricao': sessao_db['descricao'],
					'capacidade': sessao_db['capacidade'],
					'sala': sessao_db['sala'],
					'tipo': sessao_db['tipo'],
					'data': datetime.fromtimestamp(long(sessao_db['timestamp_start'])).date(),
					'hora_inicio': datetime.fromtimestamp(long(sessao_db['timestamp_start'])).time(),
					'hora_fim': datetime.fromtimestamp(long(sessao_db['timestamp_end'])).time(),
				}
			)
			# prepara o conteúdo que vai pro template
			# pra chamar no template tem que usar o nome que está entre aspas simples
			contexto = {
				'form': form,
				'topicos': topicos_sessao,
				'topicos_interesse': json.dumps(topicos_interesse)
			}
		except Exception as e:
			# do contrário, será retornado um form vazio, que será usado para a criação de um novo objeto
			form = SessaoForm()

			contexto = {
				'form': form,
				'topicos_interesse': json.dumps(topicos_interesse)
			}

		return render(request, 'sessao.html', contexto)

	@method_decorator(login_required)
	def post(self, request):
		form = SessaoForm(request.POST or None)
		if form.is_valid():
			# vai pegar a key, mesmo que ela seja vazia
			# caso a key seja vazia, é sinal de que o objeto é novo
			key = form.cleaned_data['key']
			# cria um objeto mas não salva, pois o commit=False
			sessao = form.save(commit=False)
			# Esse objeto não tem uma key definida, mas a key existe no formulário
			# Então é necessário passar a key da sessão editada para que uma nova sessão não seja criada
			sessao.key = key
			# salva o objeto editado
			sessao.save()
			logger.info('Sessão salva com sucesso: %s', sessao.key)
			messages.success(request, 'Sessão salva com sucesso!')

			if 'salvar' in request.POST:
				url = reverse('sessao')
				return redirect(url + '?key=' + sessao.key)
			elif 'salvar_novo' in request.POST:
				return redirect('sessao')
			else:
				return redirect('sessoes')
		else:
			contexto = {'form': form}
			return render(request, 'sessao.html', contexto)

@login_required
def excluir_sessao(request):
	if request.method == 'POST':
		db = firebase.database()
		item = request.POST.get('item')
		sessao = db.child('sessoes').child(item).get().val()
		if sessao:
			db.child('sessoes').child(item).remove()
			messages.success(request, 'Sessão removida com sucesso!')
			return HttpResponse('True')
		else:
			messages.error(request, 'Erro ao remover a sessão!')
			return HttpResponse('False')
	else:
		return redirect('sessoes')

@login_required
def sessoes(request):
	db = firebase.database()
	sessoes_fb = db.child('sessoes').get().val()
	sessoes = []

	try:
		for pk, s in sessoes_fb.items():
			try:
				sessao = {}
				sessao['key'] = pk
				sessao['nome'] = s['nome']
				sessao['descricao'] = s['descricao']
				sessao['capacidade'] = s['capacidade']
				sessao['sala'] = s['sala']
				sessao['tipo'] = s['tipo']
				sessoes.append(sessao)
			except KeyError as e:
				logger.error('Não foi possível carregar a sessão: %s', e)
	except AttributeError as e:
		logger.info('Não existem sessões cadastradas!')
		messages.info(request, 'Não existem sessões cadastradas!')

	contexto = {'sessoes': sessoes}

	return render(request, 'sessoes.html', contexto)
