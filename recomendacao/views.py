# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.cache import cache

from firebase.backend import firebase, fb_admin
from firebase_admin import auth as admin_auth
from datetime import datetime
import numpy as np
import json

logger = logging.getLogger(__name__)

auth = firebase.auth()
db = firebase.database()
topicos_interesse = db.child('topicos_interesse').get().val()
sessoes = {}

# Cria um stream que é atualizado sempre que tem uma mudança no firebase
# quando o servidor inicia, a cache é criada e só muda se tiver alteração no firebase
def __stream_sessoes__(message):
	sessoes = db.child('sessoes').get().val()
	cache.set('sessoes', sessoes, None)

# Inicializa o stream
sessoes_stream_handler = db.child('sessoes').stream(__stream_sessoes__)

def __get_favoritos__(dados_usuario, topicos_favoritados):
	try:
		sessoes = cache.get('sessoes')
		for sessao_favoritada in dados_usuario['favoritos']:
			try:
				topicos_sessao_favoritada = sessoes[sessao_favoritada]['topicos_interesse']
				try:
					for key, topico in topicos_sessao_favoritada.items():
						topicos_favoritados[int(key)] = 1
				except Exception as e:
					for key, topico in enumerate(topicos_sessao_favoritada):
						if topico != None:
							topicos_favoritados[int(key)] = 1
			except Exception as e:
				logger.warning('Sem tópicos nas sessões favoritadas: %s', e)
	except Exception as e:
		logger.warning('Problema criando vetor de sessões favoritadas: %s', e)

def __get_negativos__(dados_usuario, topicos_negativados):
	try:
		sessoes = cache.get('sessoes')
		for sessao_negativada in dados_usuario['recomendacoes_negativas']:
			try:
				topicos_sessao_negativada = sessoes[sessao_negativada]['topicos_interesse']

				try:
					for key, topico in topicos_sessao_negativada.items():
						topicos_negativados[int(key)] = 1
				except Exception as e:
					for key, topico in enumerate(topicos_sessao_negativada):
						if topico != None:
							topicos_negativados[int(key)] = 1
			except Exception as e:
				logger.warning('Sem tópicos nas sessões negativadas: %s', e)
	except Exception as e:
		logger.warning('Problema criando vetor de sessões negativadas: %s', e)

def __get_vetor_usuario__(dados_usuario, vetor_usuario):
	try:
		if dados_usuario['insertTopics'] == False:
			vetor_topicos = dados_usuario['topicos_interesse']
			try:
				for key, topico in vetor_topicos.items():
					vetor_usuario[int(key)] = 1
			except Exception as e:
				for key, topico in enumerate(vetor_topicos):
					if topico != None:
						vetor_usuario[int(key)] = 1
	except Exception as e:
		logger.warning('Problema criando vetor do usuário: %s', e)

def __get_positivos__(dados_usuario, topicos_positivados):
	try:
		sessoes = cache.get('sessoes')
		for sessao_positivada in dados_usuario['recomendacoes_positivas']:
			try:
				topicos_sessao_positivada = sessoes[sessao_positivada]['topicos_interesse']
				try:
					for key, topico in topicos_sessao_positivada.items():
						topicos_positivados[int(key)] = 1
				except Exception as e:
					for key, topico in enumerate(topicos_sessao_positivada):
						if topico != None:
							topicos_positivados[int(key)] = 1
			except Exception as e:
				logger.warning('Sem tópicos nas sessões positivadas: %s', e)
	except Exception as e:
		logger.warning('Problema criando vetor de sessões positivadas: %s', e)

def get_sessoes(request):

	try:
		id_token = request.GET.get('id_token')
		decoded = admin_auth.verify_id_token(id_token)

		sessoes = cache.get('sessoes')
		user = db.child('usuarios/' + decoded['user_id']).get()
		dados_usuario = user.val()

		vetor_usuario = []
		topicos_favoritados = []
		topicos_negativados = []
		topicos_positivados = []

		for i in range(0, len(topicos_interesse)):
			vetor_usuario.append(0)
			topicos_favoritados.append(0)
			topicos_negativados.append(0)
			topicos_positivados.append(0)

		vetor_usuario = np.array(vetor_usuario)
		topicos_favoritados = np.array(topicos_favoritados)
		topicos_negativados = np.array(topicos_negativados)
		topicos_positivados = np.array(topicos_positivados)

		__get_vetor_usuario__(dados_usuario, vetor_usuario)
		__get_favoritos__(dados_usuario, topicos_favoritados)
		__get_negativos__(dados_usuario, topicos_negativados)
		__get_positivos__(dados_usuario, topicos_positivados)

		sessoes_horario = {}

		hoje = datetime.now().date()
		agora = datetime.now().time()
		vetor_sessoes = {}

		try:
			recomendacao_atual = dados_usuario['recomendados'][hoje.strftime("%s")]
		except Exception as e:
			recomendacao_atual = None

		for horario in settings.HORARIOS_SESSOES:
			sessoes_horario[horario] = []
			vetor_sessoes[horario] = []

		# Cria um dict que separa as sessões por horário
		for key_s, sessao in sessoes.items():
			timestamp = sessao['timestamp_start']
			dia_completo = datetime.fromtimestamp(int(timestamp))
			dia = dia_completo.date()
			inicio = dia_completo.time()

			if dia == hoje and (inicio >= agora or not recomendacao_atual):
				horario = str(datetime.fromtimestamp(int(timestamp)).time())
				if horario in settings.HORARIOS_SESSOES:
					sessoes_horario[horario].append(key_s)
					try:
						topicos_sessao = sessao['topicos_interesse']
					except Exception as e:
						topicos_sessao = {}

					vetor_sessao = [ 0 for i in range(0, len(topicos_interesse)) ]

					try:
						for key, _ in topicos_sessao.items():
							vetor_sessao[int(key)] = 1
					except AttributeError as e:
						for key, _ in enumerate(topicos_sessao):
							vetor_sessao[key] = 1

					vetor_sessoes[horario].append(vetor_sessao)

		# converte todos os vetores das sessoes para np.array
		for horario in settings.HORARIOS_SESSOES:
			vetor_sessoes[horario] = np.array(vetor_sessoes[horario])

		# pega o vetor de negativados e retira os itens que estão no vetor de interesse
		negativar_filtro = (topicos_negativados - vetor_usuario).clip(min=0)
		# pega o vetor resultante e retira os itens favoritados
		negativar_filtro = (negativar_filtro - topicos_favoritados).clip(min=0)

		wc = vetor_usuario
		wf = topicos_favoritados
		wp = topicos_positivados
		wn = negativar_filtro

		quad_usuario = np.sqrt((np.power(wc, 2)).sum())
		quad_fav = np.sqrt((np.power(wf, 2)).sum())
		quad_pos = np.sqrt((np.power(wp, 2)).sum())
		quad_neg = np.sqrt((np.power(wn, 2)).sum())

		if recomendacao_atual:
			recomendados = recomendacao_atual
		else:
			recomendados = {}
		pontuacao = {}

		for horario, vetor_sessao in vetor_sessoes.items():
			hora = datetime.strptime(horario, "%H:%M:%S").time()
			timestamp = int(datetime.combine(hoje, hora).strftime("%s"))
			pontuacao[timestamp] = {}
			for i, sessao in enumerate(vetor_sessao):
				ws = sessao

				soma = (wc*ws).sum()
				soma_fav = (wf*ws).sum()
				soma_pos = (wp*ws).sum()
				soma_neg = (wn*ws).sum()

				quad_documento = np.sqrt((np.power(ws, 2)).sum())

				# tratar divisão por zero
				quad = quad_usuario * quad_documento
				if quad == 0:
					similaridade = 0
				else:
					similaridade = soma / quad

				quad = quad_fav * quad_documento
				if quad == 0:
					similaridade_fav = 0
				else:
					similaridade_fav = soma_fav / quad

				qud = quad_pos * quad_documento
				if quad == 0:
					similaridade_pos = 0
				else:
					similaridade_pos = soma_pos / quad

				quad = quad_neg * quad_documento
				if quad == 0:
					similaridade_neg = 0
				else:
					similaridade_neg = soma_neg / quad

				similaridade = np.around(similaridade, decimals=5)
				similaridade_fav = np.around(similaridade_fav, decimals=5)
				similaridade_pos = np.around(similaridade_pos, decimals=5)
				similaridade_neg = np.around(similaridade_neg, decimals=5)

				if similaridade_neg == 0:
					media = 0.4 * similaridade + 0.2 * similaridade_fav + 0.2 * similaridade_pos
				else:
					media = 0.4 * similaridade + 0.2 * (1-similaridade_neg) + 0.2 * similaridade_fav + 0.2 * similaridade_pos
				media = np.around(media, decimals=5)

				sessao_key = sessoes_horario[horario][i]
				pontuacao[timestamp][sessao_key] = media

			try:
				recomendado = max(pontuacao[timestamp], key=pontuacao[timestamp].get)
				if (pontuacao[timestamp][recomendado] > 0) or (pontuacao[timestamp][recomendado] == 0 and sessoes[recomendado]['tipo'] == 'Keynote'):
					recomendados[str(timestamp)] = {
						recomendado: pontuacao[timestamp][recomendado],
					}
				else:
					del recomendados[str(timestamp)]
			except Exception as e:
				logger.warning('Problema ao criar a recomendação: %s', e)

		db.child('usuarios/' + decoded['user_id'] + '/recomendados/' + hoje.strftime("%s")).set(recomendados)

		if recomendados:
			return JsonResponse(recomendados, safe=False, status=200)
		else:
			return JsonResponse({'message': 'Sem recomendações hoje'}, safe=False, status=204)

	except Exception as e:
		return JsonResponse({'message': 'Não Autorizado'}, safe=False, status=401)
