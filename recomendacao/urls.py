from django.conf.urls import url

from .views import get_sessoes

urlpatterns = [
	url(r'^recomendacao/$', get_sessoes, name='get_sessoes'),
]
