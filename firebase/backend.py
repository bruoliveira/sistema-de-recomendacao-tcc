import pyrebase
import firebase_admin
from firebase_admin import credentials
from django.conf import settings

firebase = pyrebase.initialize_app(settings.CONFIG_FIREBASE)

cred = credentials.Certificate(settings.FIREBASE_CREDENTIALS_FILE)

fb_admin = firebase_admin.initialize_app(cred)
