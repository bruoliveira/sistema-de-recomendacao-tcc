# Sistema de Recomendação

Trabalho de Conclusão de Curso

# Instruções

## Instalar o virtualenv
`sudo -H pip install virtualenv`

## Criar virtualenv
`virtualenv IHC`

## Usar virtualenv IHC
`. IHC/bin/activate` ou `source IHC/bin/activate`

## Instalar os pacotes
`pip install -r requirements.txt`

## Iniciar Servidor
`./manage.py runserver`

## Migrar banco de dados
`./manage.py migrate`

## Criar um super usuário (necessário para cadastro das sessões/tópicos/trabalhos)
`./manage.py createsuperuser`

# Credenciais do Firebase

É necessário fazer o download das credenciais do Firebase para utilizar o sistema de recomendação. As credenciais devem estar no formato json.
Informar o caminho no arquivo `settings.py`, bem como as demais informações de autenticação.

